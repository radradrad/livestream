### livestreams

Livestreams is a REST API that communicates with `radradrad/transcoder` to store and manage livestreams from rad.

Here's a small overview:

![rad diagram](rad-diagram.png)

Tools used:

- cats: Cats is used both under the hood (htt4ps) and explicitely.
- Doobie: is used as the functional abstraction on top of JDBC.
- htt4s: Used for the http server
- Quill: Use as the QDSL and for its serialisation capabilities (makes it easier to serialise and de-serialise objects to and from the DB).
- Circe: For serialisation
- ScalaTest: For testing
- Fly: Migrations

This projects also use Apache Kafka to communicate with `radradrad/transcoder`.

Please not that this is not production ready and the purpose of this repo is mainly experimental and for educationl purposes. 

TODO: 
- [ ] Add tests
- [ ] Swap MySQL for Postgres
- [ ] Refactor error handling
- [ ] Deploy to k8s together with Kafka and Postgres
