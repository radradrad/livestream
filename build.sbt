ThisBuild / scalaVersion := "2.13.10"

val Http4sVersion = "1.0.0-M21"
val CirceVersion = "0.14.0-M5"
val QuillVersion = "4.6.0"
val DoobieVersion = "1.0.0-RC1"
val LogbackVersion = "1.2.6"

enablePlugins(FlywayPlugin)
flywayUrl := "jdbc:postgresql://localhost:5432/livestreams"
flywayUser := "postgres"
flywayPassword := "postgres"

lazy val livestreams = (project in file("."))
  .settings(
    name := "livestreams",
    resolvers += "confluent" at "https://packages.confluent.io/maven/",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-ember-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "io.circe" %% "circe-generic" % CirceVersion,
      "org.tpolecat" %% "doobie-core" % DoobieVersion,
      "org.tpolecat" %%  "doobie-postgres" % DoobieVersion,
      "io.getquill" %% "quill-doobie" % QuillVersion,
      "org.postgresql" % "postgresql" % "42.6.0",
      "org.flywaydb" % "flyway-core" % "9.19.3",
      "ch.qos.logback" % "logback-classic" % "1.3.5",
      "org.specs2" %% "specs2-core" % "4.20.0" % "test",
      "com.github.geirolz" %% "fly4s-core" % "0.0.17",
      "co.fs2" %% "fs2-core" % "3.7.0",
      "com.github.fd4s" %% "fs2-kafka" % "3.0.1",
    ),
    dependencyOverrides += "org.tpolecat" %% "doobie-core" % "1.0.0-RC2"
  )
