package livestreams.db

import fly4s.core._
import fly4s.core.data._
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.effect.kernel.Sync
import cats.effect.kernel.Async
import livestreams.config.DbConfig


object Migrations {
  def migrateDb[F[_]: Async](
      dbConfig: DbConfig
  ): Resource[F, MigrateResult] =
    Fly4s
      .make[F](
        url = dbConfig.url,
        user = Some(dbConfig.user),
        password = Some(dbConfig.password.toCharArray()),
        config = Fly4sConfig(
          table = dbConfig.migrationsTable,
          validateMigrationNaming = true,
          baselineOnMigrate = true
        )
      )
      .evalMap(_.migrate)
}
