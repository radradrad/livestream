package livestreams

import doobie._
import doobie.implicits._
import cats.effect._

object Main extends IOApp {
  def run(args: List[String]) = {
    Server.build[IO].use(_ => IO.never).as(ExitCode.Success)
  }
}
