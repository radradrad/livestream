package livestreams

import fs2.kafka._
import cats.effect._
import cats.implicits._

class Producer[F[_]: Async] {
  val producerSettings =
    ProducerSettings[F, String, String]
      .withBootstrapServers(sys.env("KAFKA_URL"))

  def send(url: String): F[Either[Throwable, Unit]] =
    KafkaProducer
      .stream(producerSettings)
      .evalMap { producer =>
        val record = ProducerRecord(sys.env("KAFKA_TOPIC"), "url", url)
        val producerRecord = ProducerRecords.one(record)
        producer.produce(producerRecord)
      }
      .compile
      .drain
      .attempt
}
