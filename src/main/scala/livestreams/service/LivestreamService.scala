package livestreams.service

import doobie.implicits._
import doobie.Transactor
import cats.syntax.either._
import cats.implicits._
import cats.effect.kernel.Async
import io.circe.generic.auto._
import io.circe.syntax._
import io.getquill.{idiom => _, _}
import io.getquill.doobie.DoobieContext
import livestreams.core.Livestream
import livestreams.Producer
import livestreams.repository.LivestreamRepo

trait LivestreamService[F[_]] {
  def create(livestream: Livestream): F[Either[String, Option[Int]]]
  def getAll(): F[Either[String, Vector[Livestream]]]
  def getById(id: Int): F[Either[String, Livestream]]
}

object LivestreamService {
  def apply[F[_]: Async](xa: Transactor[F]): LivestreamService[F] = {
    new LivestreamService[F] {
      val livestreams = LivestreamRepo[F](xa)
      override def create(
          livestream: Livestream
      ): F[Either[String, Option[Int]]] = livestreams.create(livestream)
      override def getAll(): F[Either[String, Vector[Livestream]]] = livestreams.getAll()
      override def getById(id: Int): F[Either[String, Livestream]] =
        livestreams.getById(id)
    }
  }

}
