package livestreams.service

import doobie.implicits._
import doobie.Transactor
import cats.syntax.either._
import cats.implicits._
import cats.effect.kernel.Async
import io.circe.generic.auto._
import io.circe.syntax._
import io.getquill.{idiom => _, _}
import io.getquill.doobie.DoobieContext
import livestreams.core.{Track, TrackError}
import livestreams.Producer
import livestreams.repository.{TrackRepo, LivestreamRepo}

trait TrackService[F[_]] {
  def create(track: Track): F[Either[TrackError, Option[Int]]]
  def getAll(): F[Either[TrackError, List[Track]]]
  def getById(id: Int): F[Either[TrackError, Track]]
  def update(id: Int, track: Track): F[Either[TrackError, Track]]
}

object TrackService {
  def apply[F[_]: Async](xa: Transactor[F]): TrackService[F] = {
    new TrackService[F] {
      val tracks = TrackRepo[F](xa)
      val livestreams = LivestreamRepo[F](xa)
      override def create(track: Track): F[Either[TrackError, Option[Int]]] = {
        for {
          livestream <- livestreams.getById(track.livestream_id)
          response <- livestream match {
            case Right(l) => {
              // todo: add enum
              /*
              pipe:
              1. create livestream
              2. if a new song is added, and livestream is "CREATED" send to transcoder
              3. once transcoder receives, it updates the livestream to "STREAMING"
                this should be idempotent
              4. if a new track comes during this period, create it with "QUEUED"
                do not send to transcoder
              5. once transcoder finishes, it updates the livestream to "WAITING"
              6. inside that update "WAITING" method, livestreams will check
                if there's any QUEUED tracks within the livestream
                if they are, send the first one to transcoder
                if not, set livestream to "CLOSED"
              7. repeat
              8. optimisation: update "WAITING" status before finalising (not necessary)
              9. distributed transcoder: add log to keep track of unfinished streamings
              10. fault tolerance: run jobs to garbage collect dead livestreams (expiration)
               */
              l.status match {
                case Some("CREATED") => {
                  tracks.create(track.copy(status = Some("Created")))
                  // send to transcoder
                }
                case Some("WAITING") => {
                  tracks.create(track.copy(status = Some("CREATED")))
                  // send to transcoder
                }
                case Some("STREAMING") =>
                  tracks.create(track.copy(status = Some("QUEUED")))
                case Some("CLOSED") =>
                  Async[F].pure(
                    Left(
                      TrackError(
                        "the livestream attached to this track is closed",
                        TrackError.ErrTrackLivestreamClosed
                      )
                    )
                  )
                // error when status is inconsistent /shouldn't happen/ invariant
                // put enum at sql level
                case _ => {
                  Async[F].pure(
                    Left(
                      TrackError(
                        "this status is not part of the invariant",
                        TrackError.ErrTrackStatusNotFound
                      )
                    )
                  )
                }
              }
            }
            // return proper error if livestream does not exists
            // return proper error when validating
            case Left(err) =>
              Async[F].delay(
                Left(
                  TrackError(
                    s"livestream with id ${track.livestream_id} not found",
                    TrackError.ErrTrackNotFound
                  )
                )
              )
          }
        } yield response
      }
      override def getAll(): F[Either[TrackError, List[Track]]] =
        tracks.getAll()
      // return proper error if track does not exist
      override def getById(id: Int): F[Either[TrackError, Track]] =
        tracks.getById(id)
      override def update(
          id: Int,
          track: Track
      ): F[Either[TrackError, Track]] = tracks.update(id, track)
    }
  }

}
