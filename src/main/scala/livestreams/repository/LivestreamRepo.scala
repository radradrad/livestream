package livestreams.repository

import doobie.implicits._
import doobie.Transactor
import cats.syntax.either._
import cats.implicits._
import cats.effect.kernel.Async
import io.circe.generic.auto._
import io.circe.syntax._
import io.getquill.{idiom => _, _}
import io.getquill.doobie.DoobieContext
import livestreams.core.Livestream
import livestreams.Producer

trait LivestreamRepo[F[_]] {
  def create(livestream: Livestream): F[Either[String, Option[Int]]]
  def getAll(): F[Either[String, Vector[Livestream]]]
  def getById(id: Int): F[Either[String, Livestream]]
}

object LivestreamRepo {
  def apply[F[_]: Async](xa: Transactor[F]): LivestreamRepo[F] = {
    new LivestreamRepo[F] {
      val ctx = new DoobieContext.Postgres(SnakeCase)
      val p = new Producer[F]()

      import ctx._
      val livestreams = quote {
        querySchema[Livestream]("livestreams")
      }

      def create(
          livestream: Livestream
      ): F[Either[String, Option[Int]]] = {
        val insertLivestream = quote {
          livestreams
            .insertValue(lift(livestream))
            .returningGenerated(r => r.id)
        }
        ctx
          .run(insertLivestream)
          .attempt
          .transact[F](xa)
          .flatMap {
            case Left(err) => Async[F].pure(Left(err.getMessage))
            case Right(id) =>
              Async[F].delay(Right(id))
            // p.send(url).flatMap {
            //   case Left(err) => Async[F].pure(Left(err.getMessage))
            //   case Right(_)        => Async[F].delay(Right(id))
            // }
          }
      }
      def getAll(): F[Either[String, Vector[Livestream]]] = {
        ctx
          .stream(livestreams)
          .compile
          .toVector
          .transact[F](xa)
          .map(l =>
            l match {
              case Nil        => Left("Not found")
              case Vector(_*) => Right(Vector())
            }
          )
      }
      def getById(id: Int): F[Either[String, Livestream]] = {
        val getById = quote {
          livestreams.filter(_.id == lift(Option(id)))
        }
        run(getById)
          .transact[F](xa)
          .map(l =>
            l match {
              case Nil      => Left("Not found")
              case List(_*) => Right(l.head)
            }
          )
      }
    }
  }

}
