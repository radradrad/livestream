package livestreams.repository

import doobie.implicits._
import doobie.Transactor
import cats.syntax.either._
import cats.implicits._
import cats.effect.kernel.Async
import io.circe.generic.auto._
import io.circe.syntax._
import io.getquill.{idiom => _, _}
import io.getquill.doobie.DoobieContext
import livestreams.core.{Track, TrackError}
import livestreams.Producer

trait TrackRepo[F[_]] {
  def create(track: Track): F[Either[TrackError, Option[Int]]]
  def getAll(): F[Either[TrackError, List[Track]]]
  def getById(id: Int): F[Either[TrackError, Track]]
  def update(id: Int, track: Track): F[Either[TrackError, Track]]
}

object TrackRepo {
  def apply[F[_]: Async](xa: Transactor[F]): TrackRepo[F] = {
    new TrackRepo[F] {
      val ctx = new DoobieContext.Postgres(SnakeCase)

      import ctx._
      val tracks = quote {
        querySchema[Track]("tracks")
      }

      override def create(track: Track): F[Either[TrackError, Option[Int]]] = {
        val insertTrack = quote {
          tracks.insertValue(lift(track)).returningGenerated(r => (r.id))
        }
        ctx
          .run(insertTrack)
          .attempt
          .transact[F](xa)
          .map {
            case Left(err) =>
              Left(
                TrackError(
                  "error while creating track",
                  TrackError.ErrTrackDbServer
                )
              )
            case Right(id) => Right(id)
          }
      }
      override def getAll(): F[Either[TrackError, List[Track]]] = {
        val getTracks = quote {
          tracks.sortBy(_.title)
        }
        ctx
          .run(getTracks)
          .attempt
          .transact[F](xa)
          .map(l =>
            l match {
              case Right(l @ List(_*)) => Right(l)
              case Left =>
                Left(
                  TrackError(
                    "error while fetching tracks",
                    TrackError.ErrTrackDbServer
                  )
                )
            }
          )
      }
      override def getById(id: Int): F[Either[TrackError, Track]] = {
        val getById = quote {
          tracks.filter(_.id == lift(Option(id)))
        }
        run(getById)
          .transact[F](xa)
          .attempt
          .map(l =>
            l match {
              case Right(List(head)) => Right(head)
              case Right(_) =>
                Left(
                  TrackError(
                    s"track with id ${id} was not found",
                    TrackError.ErrTrackNotFound
                  )
                )
              case Left(err) =>
                Left(
                  TrackError(
                    s"error while finding track with id ${id}",
                    TrackError.ErrTrackDbServer
                  )
                )
            }
          )
      }
      override def update(
          id: Int,
          track: Track
      ): F[Either[TrackError, Track]] = {
        val update = quote {
          tracks
            .filter(p => p.id == lift(Option(id)))
            .update(
              _.title -> lift(track.title),
              _.url -> lift(track.url),
              _.status -> lift(track.status),
              _.cover -> lift(track.cover)
            )
            .returning(r => r)
        }
        run(update)
          .transact[F](xa)
          .attempt
          .map(nRows =>
            nRows match {
              case Left(err) =>
                Left(
                  TrackError(
                    s"error while updating track with id ${id}",
                    TrackError.ErrTrackDbServer
                  )
                )
              case Right(track) => Right(track)
            }
          )

      }
    }
  }

}
