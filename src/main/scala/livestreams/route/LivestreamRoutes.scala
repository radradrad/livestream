package livestreams.routes

import cats.effect.Concurrent
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes}
import org.http4s.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import cats.syntax.all._
import livestreams.core.Livestream
import livestreams.service.LivestreamService

object LivestreamRoutes {
  def apply[F[_]: Concurrent](
      livestreams: LivestreamService[F]
  ): HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._

    // Define routes
    HttpRoutes.of[F] {
      case GET -> Root / "livestreams" => {
        for {
          livestream <- livestreams.getAll()
          response <- livestream match {
            case Right(l)  => Ok(l.asJson)
            case Left(err) => Ok(Vector.empty.asJson)
          }
        } yield response
      }
      case GET -> Root / "livestreams" / livestreamId => {
        for {
          livestream <- livestreams.getById(livestreamId.toInt)
          response <- livestream match {
            case Right(l)  => Ok(l.asJson)
            case Left(err) => NotFound()
          }
        } yield response
      }
      case request @ POST -> Root / "livestreams" => {
        for {
          payload <- request.as[Livestream]
          livestream <- livestreams.create(payload)
          response <- livestream match {
            case Right(l)    => Created(l.asJson)
            case Left(state) => NotFound(state)
          }
        } yield response
      }
      case GET -> Root / ".health" => {
        Ok()
      }
    }
  }
}
