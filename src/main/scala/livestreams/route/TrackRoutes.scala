package livestreams.routes

import cats.effect.Concurrent
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes}
import org.http4s.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import cats.syntax.all._
import livestreams.core.{Track, TrackError}
import livestreams.service.TrackService
import org.http4s.Response

object TrackRoutes {
  def apply[F[_]: Concurrent](
      tracks: TrackService[F]
  ): HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._

    def handleError(errorResponse: TrackError): F[Response[F]] = {
      errorResponse.error match {
        case TrackError.ErrTrackNotFound => NotFound(errorResponse.asJson)
        case TrackError.ErrTrackDbServer =>
          InternalServerError(errorResponse.asJson)
        case TrackError.ErrTrackStatusNotFound => NotFound(errorResponse.asJson)
        case TrackError.ErrTrackLivestreamClosed =>
          BadRequest(errorResponse.asJson)
      }
    }

    // Define routes
    HttpRoutes.of[F] {
      case GET -> Root / "tracks" => {
        for {
          track <- tracks.getAll()
          response <- track match {
            case Right(t)      => Ok(t.asJson)
            case Left(message) => handleError(message)
          }
        } yield response
      }
      case GET -> Root / "tracks" / trackId => {
        for {
          track <- tracks.getById(trackId.toInt)
          response <- track match {
            case Right(t)      => Ok(t.asJson)
            case Left(message) => handleError(message)
          }
        } yield response
      }
      case request @ POST -> Root / "tracks" => {
        for {
          payload <- request.as[Track]
          track <- tracks.create(payload)
          response <- track match {
            case Right(t)      => Created(t.asJson)
            case Left(message) => handleError(message)
          }
        } yield response
      }
      case request @ PUT -> Root / "tracks" / trackId => {
        for {
          payload <- request.as[Track]
          track <- tracks.update(trackId.toInt, payload)
          response <- track match {
            case Right(t)      => Ok(t.asJson)
            case Left(message) => handleError(message)
          }
        } yield response
      }
    }
  }
}
