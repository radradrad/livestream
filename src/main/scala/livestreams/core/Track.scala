package livestreams.core

import cats.effect.Concurrent
import io.circe.generic.auto._
import org.http4s.EntityDecoder
import org.http4s.circe._
import io.circe.Encoder
import io.circe.syntax._
import io.circe.Decoder

final case class Track(
    id: Option[Int] = None,
    title: String,
    url: String,
    cover: Option[String] = None,
    status: Option[String] = None,
    created_at: Option[String] = None,
    livestream_id: Int
)

object Track {
  implicit def trackDecoder[F[_]: Concurrent]: EntityDecoder[F, Track] =
    jsonOf[F, Track]
  implicit def trackVectorDecoder[F[_]: Concurrent]
      : EntityDecoder[F, Vector[Track]] =
    jsonOf[F, Vector[Track]]
}

final case class TrackError(message: String, error: TrackError.Value)
object TrackError extends Enumeration {
  val ErrTrackNotFound, ErrTrackDbServer, ErrTrackStatusNotFound,
      ErrTrackLivestreamClosed = Value

  implicit val trackErrorValueEncoder: Encoder[TrackError.Value] =
    Encoder.instance(_.toString.asJson)
}
