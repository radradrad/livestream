package livestreams.core

import org.http4s.EntityDecoder
import io.circe.generic.auto._
import cats.effect.Concurrent
import org.http4s.circe._

// todo: add datetime
case class Livestream(
    id: Option[Int] = None,
    name: String,
    status: Option[String] = None,
    expires_at: Option[String] = None,
    created_at: Option[String] = None
)

// todo: add datetime
object Livestream {
  implicit def livestreamDecoder[F[_]: Concurrent]
      : EntityDecoder[F, Livestream] =
    jsonOf[F, Livestream]
  implicit def livestreamVectorDecoder[F[_]: Concurrent]
      : EntityDecoder[F, Vector[Livestream]] =
    jsonOf[F, Vector[Livestream]]
}
