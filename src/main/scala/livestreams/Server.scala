package livestreams

import cats.effect.Async
import cats.effect.IO
import cats.effect.Resource
import cats.effect.kernel.Sync
import cats.implicits._
import com.comcast.ip4s._
import doobie._
import doobie.implicits._
import livestreams.config.DbConfig
import livestreams.db.Migrations
import livestreams.service.LivestreamService
import livestreams.service.TrackService
import livestreams.routes.LivestreamRoutes
import livestreams.routes.TrackRoutes
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.middleware.CORS
import org.http4s.server.middleware.CORSConfig
import org.http4s.server.middleware.Logger
import scala.concurrent.duration._

object Server {
  def build[F[_]: Async]: Resource[F, Server] = {
    // will refactor
    val dbConfig: DbConfig = DbConfig(
      url = sys.env("MYSQL_URL"),
      user = sys.env("MYSQL_USER"),
      password = sys.env("MYSQL_PASS"),
      migrationsTable = "flyway"
    )
    val xa = Transactor.fromDriverManager[F](
      "org.postgresql.Driver",
      dbConfig.url,
      dbConfig.user,
      dbConfig.password
    )
    val originConfig = CORSConfig(
      anyOrigin = true,
      // allowedOrigins = Set("*"),
      allowCredentials = false,
      maxAge = 1.day.toSeconds
    )

    val livestreamService = LivestreamService[F](xa)
    val trackService = TrackService[F](xa)

    val routes = {
      LivestreamRoutes[F](livestreamService) <+> TrackRoutes[F](trackService)
    }

    val httpApp =
      CORS(Logger.httpApp(true, true)(routes.orNotFound), originConfig)

    for {
      _ <- Migrations.migrateDb(dbConfig)
      server <- EmberServerBuilder
        .default[F]
        .withHost(ipv4"0.0.0.0")
        .withPort(port"8080")
        .withHttpApp(httpApp)
        .build
    } yield server
  }
}
