CREATE TABLE "livestreams" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(30),
    "status" VARCHAR(30),
    "created_at" VARCHAR(30),
    "expires_at" VARCHAR(30)
);

CREATE TABLE "tracks" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "title" VARCHAR(30),
    "url" VARCHAR(100),
    "cover" VARCHAR(100),
    "status" VARCHAR(30),
    "created_at" VARCHAR(30),
    "livestream_id" BIGINT,
    FOREIGN KEY ("livestream_id") REFERENCES "livestreams"("id") 
);
