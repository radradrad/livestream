package livestreams.routes

import org.specs2.mutable._
import org.specs2.specification.{Scope, BeforeEach}
import cats.effect.{IO, Sync}
import cats.effect.unsafe.implicits.global

import cats.effect.Async
import org.http4s.Status
import org.http4s.HttpApp
import org.http4s.circe._
import org.http4s._
import org.http4s.LiteralSyntaxMacros._
import org.http4s.implicits._
import io.circe.syntax._
import io.circe.generic.auto._
import livestreams.core.{Track, Livestream}
import livestreams.Context
import livestreams.{Context, RoutesSpecSupport}
import org.http4s.EntityDecoder._
import org.http4s.circe.CirceEntityCodec._

class TrackRoutesSpec extends Specification with RoutesSpecSupport {
  "create track returns 201" in new Context {
    val trackCreated = createTrack()
    trackCreated.status must beEqualTo(Status.Created)

    val trackId = trackCreated.as[Int].unsafeRunSync()
    // val trackCreated = response.as[Track].unsafeRunSync()
    // trackCreated.title must beEqualTo(trackFix.title)
    // trackCreated.url must beEqualTo(trackFix.url)
    // trackCreated.status must beEqualTo(trackFix.status)
  }

  // TODO test error
  // "get a track by id returns correct track" in new Context {

  "get a track by id returns correct track" in new Context {
    val trackCreated = createTrack()
    val trackId = trackCreated.as[Int].unsafeRunSync()

    val getById =
      Request[IO](Method.GET, uri"/tracks" / trackId.toString)
    val response = httpApp.run(getById).unsafeRunSync()
    response.status must beEqualTo(Status.Ok)

    val res = response.as[Track].unsafeRunSync
    res.title must beEqualTo(trackFix.title)
    res.url must beEqualTo(trackFix.url)
    res.status must beEqualTo(Some("QUEUED"))
  }

  // "get all tracks" in new Context {
  //   createTrack()
  //   val getById =
  //     Request[IO](Method.GET, uri"/tracks")
  //   val response = httpApp.run(getById).unsafeRunSync()
  //   response.status must beEqualTo(Status.Ok)
  //   val res = response.as[Vector[Track]].unsafeRunSync()
  //   res.map(rL => {
  //     rL.title must beEqualTo(trackFix.title)
  //     rL.url must beEqualTo(trackFix.url)
  //   })
    // BeforeEach runs concurrently
    // res.length must beEqualTo(2)
  // }

  // "update track returns 200" in new Context {
  //   val trackCreated = createTrack()
  //   val trackId = trackCreated.as[Int].unsafeRunSync()
  //   val newTrack = trackFix.copy(status = Some("STREAMING"))
  //   val update =
  //     Request[IO](Method.PUT, uri"/tracks" / "1").withEntity(newTrack.asJson)
  //   val response = httpApp.run(update).unsafeRunSync()
  //   response.status must beEqualTo(Status.Ok)
  // }

  def createTrack(track: Track = trackFix): Response[IO] = {
    val livestreamCreated = createLivestream(livestreamFix)
    val livestreamId = livestreamCreated.as[Int].unsafeRunSync()
    val postTrack =
      Request[IO](Method.POST, uri"/tracks").withEntity(
        track.copy(livestream_id = livestreamId).asJson
      )
    httpApp.run(postTrack).unsafeRunSync()
  }

  def createLivestream(livestream: Livestream): Response[IO] = {
    val postLivestream =
      Request[IO](Method.POST, uri"/livestreams").withEntity(livestream.asJson)
    httpApp.run(postLivestream).unsafeRunSync()
  }
}
