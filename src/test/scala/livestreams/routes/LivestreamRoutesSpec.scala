package livestreams.routes

import org.specs2.mutable._
import org.specs2.specification.{Scope, BeforeEach}
import cats.effect.{IO, Sync}
import cats.effect.unsafe.implicits.global

import cats.effect.Async
import org.http4s.Status
import org.http4s.HttpApp
import org.http4s.circe._
import org.http4s._
import org.http4s.LiteralSyntaxMacros._
import org.http4s.implicits._
import io.circe.syntax._
import io.circe.generic.auto._
import livestreams.core.Livestream
import livestreams.{Context, RoutesSpecSupport}
import org.http4s.EntityDecoder._
import org.http4s.circe.CirceEntityCodec._

class LivestreamRoutesSpec extends Specification with RoutesSpecSupport {
  "create livestream returns 201" in new Context {
    val response = createLivestream(livestreamFix)
    response.status must beEqualTo(Status.Created)
    // val livestreamCreated = response.as[Livestream].unsafeRunSync()
    // livestreamCreated.name must beEqualTo(livestreamFix.name)
  }

  "get a livestream by id returns correct livestream" in new Context {
    val livestreamCreated = createLivestream(livestreamFix)
    val id = livestreamCreated.as[Int].unsafeRunSync()

    val getById =
      Request[IO](Method.GET, uri"/livestreams" / id.toString)

    val response = httpApp.run(getById).unsafeRunSync()
    response.status must beEqualTo(Status.Ok)

    val livestreamFound = response.as[Livestream].unsafeRunSync
    livestreamFound.name must beEqualTo(livestreamFix.name)
    livestreamFound.id must beEqualTo(Some(id))
  }

  // TODO test error
  // "get a livestream by id returns correct livestream" in new Context {

  "get all livestreams" in new Context {
    createLivestream(livestreamFix)
    val getById =
      Request[IO](Method.GET, uri"/livestreams")
    val response = httpApp.run(getById).unsafeRunSync()
    response.status must beEqualTo(Status.Ok)
    val res = response.as[Vector[Livestream]].unsafeRunSync()
    res.map(rL => {
      rL.name must beEqualTo(livestreamFix.name)
    })
    // BeforeEach runs concurrently
    // res.length must beEqualTo(3)
  }

  ".health returns 200" in new Context {
    val health =
      Request[IO](Method.GET, uri"/.health")
    val response = httpApp.run(health).unsafeRunSync()
    response.status must beEqualTo(Status.Ok)
  }

  def createLivestream(livestream: Livestream): Response[IO] = {
    val postLivestream =
      Request[IO](Method.POST, uri"/livestreams").withEntity(livestream.asJson)
    httpApp.run(postLivestream).unsafeRunSync()
  }
}
