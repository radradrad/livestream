package livestreams

import doobie._
import doobie.implicits._
import com.comcast.ip4s._
import cats.effect.IO
import org.specs2.specification.{Scope, BeforeAfterEach}
import cats.effect.unsafe.implicits.global

import org.http4s.HttpApp
import org.http4s._
import org.http4s.implicits._
import org.http4s.LiteralSyntaxMacros._
import livestreams.config.DbConfig
import livestreams.service.{TrackService, LivestreamService}
import livestreams.routes.{TrackRoutes, LivestreamRoutes}
import livestreams.core.{Track, Livestream}
import livestreams.db.Migrations

trait Context extends Scope {
  val url = "jdbc:postgresql://localhost:5432/test"
    val dbConfig: DbConfig = DbConfig(
      url = url,
      user = "postgres",
      password = "postgres",
      migrationsTable = "flyway"
    )
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      dbConfig.url,
      dbConfig.user,
      dbConfig.password
    )

  val result = Migrations.migrateDb[IO](dbConfig).use { migrateResult =>
    IO.pure(migrateResult)
  }
  result.unsafeRunSync()

  val livestreamService = LivestreamService[IO](xa)
  val trackService = TrackService[IO](xa)
  val routes = {
    import cats.syntax.semigroupk._
    LivestreamRoutes[IO](livestreamService) <+> TrackRoutes[IO](trackService)
  }
  val httpApp: HttpApp[IO] = routes.orNotFound

  val livestreamFix = Livestream(
    name = "title",
    status = Some("STREAMING"),
    expires_at = Some("hi"),
    created_at = Some("hi")
  )
  val trackFix = Track(
    title = "title",
    url = "soundcloud.com/",
    status = Some("CREATED"),
    cover = Some("hi"),
    created_at = Some("hi"),
    livestream_id = 1
  )
}

trait RoutesSpecSupport extends BeforeAfterEach with Context {
  def before: Unit = {
    val y = xa.yolo
    import y._
    sql"DELETE FROM tracks".update.run.transact(xa).unsafeRunSync()
    sql"DELETE FROM livestreams".update.run.transact(xa).unsafeRunSync()
  }

  def after: Unit = {
    val y = xa.yolo
    import y._
    sql"DELETE FROM tracks".update.run.transact(xa).unsafeRunSync()
    sql"DELETE FROM livestreams".update.run.transact(xa).unsafeRunSync()
  }
}
